package com.featzima.domain;

import java.util.UUID;

public class Note {

    private final UUID id;
    private final String title;
    private final String content;

    public Note(UUID id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
