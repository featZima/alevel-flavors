package com.featzima.domain;

public interface UserPreferences {

    void setPreferedDate(PreferedDate preferedDate);

    PreferedDate getPreferedDate();

}
