package com.featzima.domain;

public class Child<T> {

    private final Changes changes;
    private final T value;

    public Child(Changes changes, T value) {
        this.changes = changes;
        this.value = value;
    }

    public Changes getChanges() {
        return changes;
    }

    public T getValue() {
        return value;
    }
}
