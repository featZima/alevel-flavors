package com.featzima.domain;

import java.util.UUID;

import io.reactivex.Observable;

public interface NotesRepository {

    Observable<Nothing> saveNote(Note note);

    Observable<Nothing> deleteNote(UUID noteId);

    Observable<Child<Note>> getNotes();
}
