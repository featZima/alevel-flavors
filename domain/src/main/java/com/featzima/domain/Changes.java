package com.featzima.domain;

public enum Changes {
    Added,
    Changed,
    Deleted
}
