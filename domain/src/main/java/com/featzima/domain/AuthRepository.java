package com.featzima.domain;


import io.reactivex.Observable;

public interface AuthRepository {

    Observable<Nothing> signUp(String userName, String password);

    Observable<Nothing> signIn(String userName, String password);

}
