package com.featzima.data_firebase;

import com.featzima.domain.AuthRepository;
import com.featzima.domain.Nothing;

import javax.inject.Inject;

import io.reactivex.Observable;

public class FirebaseAuthRepository implements AuthRepository {

    @Inject
    public FirebaseAuthRepository() {
    }

    @Override
    public Observable<Nothing> signUp(String userName, String password) {
        return null;
    }

    @Override
    public Observable<Nothing> signIn(String userName, String password) {
        return null;
    }
}
