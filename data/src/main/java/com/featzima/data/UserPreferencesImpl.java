package com.featzima.data;

import android.content.Context;

import com.featzima.domain.PreferedDate;
import com.featzima.domain.UserPreferences;

import javax.inject.Inject;

public class UserPreferencesImpl implements UserPreferences {

    private final Context context;

    @Inject
    public UserPreferencesImpl(Context context) {
        this.context = context;
    }

    @Override
    public void setPreferedDate(PreferedDate preferedDate) {
        context.getSharedPreferences("user-preferences", 0)
                .edit()
                .putInt("prefered-date", preferedDate.ordinal())
                .commit();
    }

    @Override
    public PreferedDate getPreferedDate() {
        return PreferedDate.values()[context.getSharedPreferences("user-preferences", 0)
                .getInt("prefered-date", 0)]
    }
}
