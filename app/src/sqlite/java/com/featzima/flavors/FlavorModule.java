package com.featzima.flavors;

import com.featzima.data_sqlite.SqliteAuthRepository;
import com.featzima.data_sqlite.SqliteNotesRepository;
import com.featzima.domain.AuthRepository;
import com.featzima.domain.NotesRepository;

import dagger.Binds;
import dagger.Module;

@Module
public interface FlavorModule {

    @Binds
    NotesRepository provideNotesRepository(SqliteNotesRepository impl);

    @Binds
    AuthRepository provideAuthRepository(SqliteAuthRepository impl);
}
