package com.featzima.flavors;

import com.featzima.data_firebase.FirebaseAuthRepository;
import com.featzima.data_firebase.FirebaseNotesRepository;
import com.featzima.domain.AuthRepository;
import com.featzima.domain.NotesRepository;

import dagger.Binds;
import dagger.Module;

@Module
public interface FlavorModule {

    @Binds
    NotesRepository provideNotesRepository(FirebaseNotesRepository impl);

    @Binds
    AuthRepository provideAuthRepository(FirebaseAuthRepository impl);
}
