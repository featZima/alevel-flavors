package com.featzima.data_sqlite;

import com.featzima.domain.Child;
import com.featzima.domain.Note;
import com.featzima.domain.NotesRepository;
import com.featzima.domain.Nothing;

import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SqliteNotesRepository implements NotesRepository {

    @Inject
    public SqliteNotesRepository() {
    }

    @Override
    public Observable<Nothing> saveNote(Note note) {
        return null;
    }

    @Override
    public Observable<Nothing> deleteNote(UUID noteId) {
        return null;
    }

    @Override
    public Observable<Child<Note>> getNotes() {
        return null;
    }
}
