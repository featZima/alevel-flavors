package com.featzima.data_sqlite;

import com.featzima.domain.AuthRepository;
import com.featzima.domain.Nothing;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SqliteAuthRepository implements AuthRepository {

    @Inject
    public SqliteAuthRepository() {
    }

    @Override
    public Observable<Nothing> signUp(String userName, String password) {
        return Observable.empty();
    }

    @Override
    public Observable<Nothing> signIn(String userName, String password) {
        return Observable.empty();
    }
}
